﻿
namespace WpfApp1
{
    class Util
    {
        public static float calculate(int operation, float number1, float number2)
        {
            float answer;
            switch (operation)
            {
                case 1:
                    answer = number1 + number2;
                    return answer;

                case 2:
                    answer = number1 - number2;
                    return answer;

                case 3:
                    answer = number1 * number2;
                    return answer;

                case 4:
                    answer = number1 / number2;
                    return answer;

                default:
                    return 0;
            }
        }
    }
}
