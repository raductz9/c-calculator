﻿using System.Windows;
using System.Windows.Controls;

namespace WpfApp1
{
    public partial class MainWindow : Window
    {
        Operations operation = new Operations();

        public MainWindow()
        {
            InitializeComponent();
            txtDisplay.Text = "";
            txtInfo.Text = "";
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            txtDisplay.Text = txtDisplay.Text + button.Content.ToString();
        }

        private void plusClick(object sender, RoutedEventArgs e)
        {
            operation.plusExecute(txtDisplay.Text);
            txtInfo.Text = txtDisplay.Text + "+";
            txtDisplay.Clear();
            txtDisplay.Focus();
        }

        private void minusClick(object sender, RoutedEventArgs e)
        {
            operation.minusExecute(txtDisplay.Text);
            txtInfo.Text = txtDisplay.Text + "-";
            txtDisplay.Clear();
            txtDisplay.Focus();
        }

        private void multiplyClick(object sender, RoutedEventArgs e)
        {
            operation.multiplyExecute(txtDisplay.Text);
            txtInfo.Text = txtDisplay.Text + "*";
            txtDisplay.Clear();
            txtDisplay.Focus();
        }

        private void divideClick(object sender, RoutedEventArgs e)
        {
            operation.divideExecute(txtDisplay.Text);
            txtInfo.Text = txtDisplay.Text + "/";
            txtDisplay.Clear();
            txtDisplay.Focus();
        }

        private void equalClick(object sender, RoutedEventArgs e)
        {
            txtDisplay.Text = operation.equalExecute(txtDisplay.Text);
            txtInfo.Text = "";
        }

        private void clearExecuteClick(object sender, RoutedEventArgs e)
        {
            txtDisplay.Clear();
            txtInfo.Clear();
        }
    }
}
