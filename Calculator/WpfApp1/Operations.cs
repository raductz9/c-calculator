﻿
namespace WpfApp1
{
    class Operations
    {
        float number1, number2;
        int operation;

        public Operations()
        {
            operation = 0;
            number1 = 0;
            number2 = 0;
        }

        public void plusExecute(string txtDisplay)
        {
            this.number1 = float.Parse(txtDisplay);
            this.operation = 1;
        }

        public void minusExecute(string txtDisplay)
        {
            this.number1 = float.Parse(txtDisplay);
            this.operation = 2;
        }

        public void multiplyExecute(string txtDisplay)
        {
            this.number1 = float.Parse(txtDisplay);
            this.operation = 3;
        }

        public void divideExecute(string txtDisplay)
        {
            this.number1 = float.Parse(txtDisplay);
            this.operation = 4;
        }

        public string equalExecute(string txtDisplay)
        {
            this.number2 = float.Parse(txtDisplay);
            if (number2 == 0)
                return "Can't divide by 0!";
            else
                return (Util.calculate(this.operation, this.number1, this.number2)).ToString();
        }
    }
}
